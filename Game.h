#pragma once

#include "types.h"

#define MAX_PLAYERS 1

struct World;

struct BackBuffer {
	u8 *buffer;
	int pitch;

	int height;
	int width;
	int bytesPerPixel;
};

struct Game {
	u8 maxPlayers = MAX_PLAYERS;
	u8 numPlayers = 0;

	bool isRunning;
	
	BackBuffer backBuffer;
	
	World *world;
};

void handle_events(Game*);
void update(Game*, u32);
void render_to_back_buffer(World *world, BackBuffer backBuffer);