#include <SDL2\SDL.h>
#include <stdio.h>

#include "Game.h"
#include "World.h"

void add_entity(Game *game) {
	Entity entity;
	entity.color = 0xFFFF00FF;
	entity.height = 150;
	entity.width = 75;
	entity.topLeft = Vector2{ 0, 0 };

	game->world->entities[game->world->numEntities++] = entity;
}

void handle_events(Game *game) {
	SDL_Event event;
	SDL_PollEvent(&event);

	switch (event.type) {
	case SDL_QUIT:
		game->isRunning = false;
		break;

	case SDL_KEYDOWN:
	{
		if (event.key.keysym.sym == SDLK_SPACE) {
			add_entity(game);
		}
	} break;

	default: break;
	}
}

void update(Game *game, u32 dt) {
}

void render_entity(BackBuffer backBuffer, Entity *entity) {
	auto minX = min(0, entity->topLeft.x);
	auto minY = min(0, entity->topLeft.y);
	auto maxX = min(backBuffer.width, entity->topLeft.x + entity->width);
	auto maxY = min(backBuffer.height, entity->topLeft.y + entity->height);

	for (u32 row = minY; row < maxY; row++) {
		for (u32 column = minX; column < maxX; column++) {
			auto pixel = (u32 *)(&backBuffer.buffer[row * backBuffer.pitch + (column * backBuffer.bytesPerPixel)]);
			*pixel = entity->color;
		}
	}
}

void render_to_back_buffer(World *world, BackBuffer backBuffer) {
	for (auto entityIndex = 0; entityIndex < world->numEntities; entityIndex++) {
		auto entity = world->entities[entityIndex];

		render_entity(backBuffer, &entity);
	}
}
