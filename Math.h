#pragma once

#include "types.h"

#define max(a, b) a > b ? a : b
#define min(a, b) a < b ? a : b

struct Vector2 {
	f32 x;
	f32 y;
};

struct Rect {
	Vector2 topLeft;
	u32 width;
	u32 height;
};

inline bool is_inside(Rect rect, Vector2 position) {
	return rect.topLeft.x + rect.width >= position.x 
		&& rect.topLeft.x <= position.x 
		&& rect.topLeft.y + rect.height >= position.y 
		&& rect.topLeft.y <= position.y;
}