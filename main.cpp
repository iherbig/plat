#include <SDL2\SDL.h>
#include <stdio.h>
#include <string.h>

#include "Game.h"
#include "World.h"

#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600
#define MAX_ENTITIES 4096

void swap_back_buffer(SDL_Renderer *renderer, SDL_Texture *screenBuffer, BackBuffer buffer) {
	void *data;
	int pitch;

	if (SDL_LockTexture(screenBuffer, NULL, &data, &pitch) < 0) {
		SDL_Log("%s\n", SDL_GetError());
		return;
	}

	memcpy(data, buffer.buffer, pitch * buffer.height);

	SDL_UnlockTexture(screenBuffer);

	if (SDL_RenderCopy(renderer, screenBuffer, NULL, NULL) < 0) {
		SDL_Log("%s\n", SDL_GetError());
	}

	SDL_RenderPresent(renderer);
}

int main(int argc, char **argv) {
	if (SDL_Init(SDL_INIT_EVERYTHING) > 0) {
		SDL_Log("SDL failed to initialize.\n");
		return -1;
	}

	auto window = SDL_CreateWindow("Plat", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINDOW_WIDTH, WINDOW_HEIGHT, 0);

	if (!window) {
		SDL_Log("Failed to create SDL window.\n");
		return -1;
	}

	World world;
	world.width = WINDOW_WIDTH;
	world.height = WINDOW_HEIGHT;
	world.entities = (Entity *)malloc(sizeof(Entity) * MAX_ENTITIES);
	world.numEntities = 0;

	Game game;
	game.isRunning = true;

	BackBuffer buffer;
	buffer.bytesPerPixel = 4;
	buffer.width = WINDOW_WIDTH;
	buffer.height = WINDOW_HEIGHT;
	buffer.buffer = (u8 *)calloc(buffer.width * buffer.height, sizeof(u32));
	buffer.pitch = buffer.bytesPerPixel * buffer.width;

	game.backBuffer = buffer;
	game.world = &world;

	auto renderer = SDL_CreateRenderer(window, -1, 0);
	auto screenBuffer = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_STREAMING, buffer.width, buffer.height);

	if (!renderer) {
		SDL_Log("Failed to create renderer.\n");
		return -1;
	}

	auto lastTick = SDL_GetTicks();
	const int targetFps = 60;
	const int targetTimePerFrame = 1000 / targetFps;
	
	SDL_SetRenderTarget(renderer, screenBuffer);

	while (game.isRunning) {
		handle_events(&game);

		auto currentTick = SDL_GetTicks();
		
		while (currentTick - lastTick < targetTimePerFrame) {
			SDL_Delay(targetTimePerFrame - (currentTick - lastTick));
			currentTick = SDL_GetTicks();
		}

		auto dt = currentTick - lastTick;
		lastTick = currentTick;

		SDL_Log("FPS: %f\n", 1000.0 / dt);
		
		update(&game, dt);

		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
		SDL_RenderClear(renderer);

		render_to_back_buffer(game.world, game.backBuffer);
		swap_back_buffer(renderer, screenBuffer, game.backBuffer);
	}

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}
