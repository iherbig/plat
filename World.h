#pragma once

#include <assert.h>

#include "types.h"
#include "Math.h"

struct Entity {
	Vector2 topLeft;
	int height;
	int width;
	u32 color;
};

struct World {
	u32 width;
	u32 height;

	int numEntities;
	Entity *entities;
};
